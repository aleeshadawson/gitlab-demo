import './index.css'

const HomePage = () => {
    return (
            <div className='content'>
                <h1>Home</h1>
                <p>This simple React application was created to demo GitLab CI/CD capabilities and compare them to Jenkins. Note that I am definitely not a designer, but I dabble a bit in front-end development.</p>  
            </div>
    );
};

export default HomePage;