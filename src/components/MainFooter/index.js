import './index.css'

const MainFooter = () => {
    return (
        <footer>
            <p>
                Dawson's GitLab Demo
            </p>
        </footer>
    )
}

export default MainFooter;