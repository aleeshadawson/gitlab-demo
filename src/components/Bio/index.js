import owner from './owner.jpeg'

import './index.css'

const AboutMe = () => {
    return (
            <div className='content'>
                <img src={owner} alt='owner'></img>
                <h1>Aleesha Dawson</h1>
                <br />
                <p class='info'>Passionate about teaching</p>
                <p class='info'>Dedicated to self-sufficiency</p>
                <p class='info'>Software enthusiast</p>   
            </div>
    );
};

export default AboutMe;