import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import App from './App';


const basename = ''


const index = basename ? 1 : 3;
let path_starts_with_hash = ''

path_starts_with_hash = window.location.href.split('/' + basename)[index].startsWith('#')

if (!path_starts_with_hash ) {
    const newHrefArray = window.location.href.split('/'+ basename)
    newHrefArray.splice(index, 0, basename + '#')
    window.location.href = newHrefArray.join('/')
}

ReactDOM.render(<Router basename={'/#'}><App /></Router>, document.getElementById('root'));
