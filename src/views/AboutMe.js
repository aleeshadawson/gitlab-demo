import Bio from '../components/Bio';
const AboutMe = () => {
    return (
        <section>
            <Bio /> 
        </section>
    );
};

export default AboutMe;