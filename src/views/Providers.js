import UnderConstruction from '../components/UnderConstruction';

const Providers = () => {
    return (
        <section>
            <UnderConstruction />
        </section>
    );
};

export default Providers;